
resource "aws_cloudwatch_log_group" "log_group" {
  name = "${var.environment}_${var.task_name}_log_group"
  retention_in_days = 1

  tags {
    Environment = "${var.environment}"
    Application = "${var.task_name}"
  }
}

/*====
ECS task definitions
======*/
data "template_file" "environment_json" {
  template = "${file("${path.module}/environment.json")}"
  count    = "${length(keys(var.container_environment_vars))}"
  vars {
    name = "${element(keys(var.container_environment_vars), count.index)}"
    value = "${lookup(var.container_environment_vars, element(keys(var.container_environment_vars), count.index))}"
  }
}

/* the task definition for the web service */
data "template_file" "web_task" {
  template = "${file("${path.module}/web_task_definition.json")}"

  vars {
    task_name                   = "${var.task_name}"
    container_port              = "${var.container_port}"
    image                       = "${var.ecr_repository_url}"
    log_group                   = "${aws_cloudwatch_log_group.log_group.name}"
    log_aws_region              = "${var.aws_region}"
    container_environment_vars  = "[${join(",", data.template_file.environment_json.*.rendered)}]"
  }
  depends_on = ["aws_cloudwatch_log_group.log_group"]
}

data "aws_ecs_task_definition" "web_task_definition" {
  task_definition = "${aws_ecs_task_definition.web_task_definition.family}"
  depends_on = ["aws_ecs_task_definition.web_task_definition"]
}

resource "aws_ecs_task_definition" "web_task_definition" {
    family                = "${var.environment}_${var.task_name}"
    container_definitions    = "${data.template_file.web_task.rendered}"
}