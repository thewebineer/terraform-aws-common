resource "aws_ecs_service" "ecs_service" {
  	name            = "${var.environment}_${var.task_name}_ecs_service"
  	iam_role        = "${aws_iam_role.ecs_service_role.name}"
  	cluster         = "${var.ecs_cluster_id}"
  	task_definition = "${aws_ecs_task_definition.web_task_definition.family}:${max("${aws_ecs_task_definition.web_task_definition.revision}", "${data.aws_ecs_task_definition.web_task_definition.revision}")}"
  	desired_count   = 2

  	load_balancer {
    	target_group_arn  = "${aws_alb_target_group.ecs_target_group.arn}"
    	container_port    = "${var.container_port}"
    	container_name    = "${var.task_name}"
	}

	
  depends_on = ["aws_ecs_task_definition.web_task_definition"]
}