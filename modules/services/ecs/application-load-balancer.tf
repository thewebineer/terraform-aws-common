resource "aws_alb" "ecs_load_balancer" {
    name                = "${var.environment}-ecs-load-balancer"
    security_groups     = ["${var.public_sg_id}"]
    subnets             = ["${var.public_subnet_ids}"]

    tags {
      Name = "${var.environment}-ecs-load-balancer"
    }
}

resource "aws_alb_target_group" "ecs_target_group" {
    name                = "${var.environment}-ecs-target-group"
    port                = "80"
    protocol            = "HTTP"
    vpc_id              = "${var.vpc_id}"

    health_check {
        healthy_threshold   = "2"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "200"
        path                = "${var.alb_health_check_path}"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags {
      Name = "${var.environment}-ecs-target-group"
    }

    depends_on = ["aws_alb.ecs_load_balancer"]
}

resource "aws_alb_listener" "alb_listener" {
    load_balancer_arn = "${aws_alb.ecs_load_balancer.arn}"
    port              = "80"
    protocol          = "HTTP"

    default_action {
        target_group_arn = "${aws_alb_target_group.ecs_target_group.arn}"
        type             = "forward"
    }

    depends_on = ["aws_alb_target_group.ecs_target_group"]
}