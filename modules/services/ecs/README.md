will drain previous containers connections which will leave original containers up until new containers stable user may see previous data for a period of time
terraform taint -module=ecs aws_ecs_task_definition.web_task_definition
terraform apply

will stop all containers and start new service rendering site down until containers come back user may see 503
terraform taint -module=ecs aws_ecs_service.ecs_service
terraform apply