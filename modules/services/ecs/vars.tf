variable "aws_region" {
  description = "The AWS region to use"
}

variable "vpc_id" {
  description = "The VPC to use"
}

variable "public_sg_id" {
  description = "security group to use"
}

variable "public_subnet_ids" {
  description = "subnet ids to use"
  type = "list"
}

variable "ecs_cluster_id" {
  description = "ecs cluster id"
}

variable "task_name" {
  description = "task name"
}

variable "container_port" {
  description = "container port"
}

variable "ecr_repository_url" {
  description = "ecr repository url"
}

variable "environment" {
  description = "environment"
}

variable "container_environment_vars" {
  description = "container environment variables"
  default = {
      "TestName1" = "TestValue1",
      "TestName2" = "TestValue2",
    }
  type = "map"
}

variable "alb_health_check_path" {
  description = "path to check for health of alb targets"
  default = "/"
}


