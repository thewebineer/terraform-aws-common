variable "environment" {
  description = "environment"
}

variable "site_name" {
  description = "site_name"
}

variable "cloudfront_price_class" {
  description = "cloud front price class for distibution caching PriceClass_All , PriceClass_200 (US,CANADA,EUROPE,ASIA,AFRICA), PriceClass_100 (US,CANADA,EUROPE)"
  default = "PriceClass_100"
}

variable "geo_restriction_whitelist" {
  default = ["US", "CA", "GB", "DE"]
  type = "list"
}

variable "use_geo_restriction_whitelist" {
  default = false
}

