
variable "aws_region" {
  description = "The AWS region to use"
}

variable "repository_name" {
  description = "ecr repository name"
}
