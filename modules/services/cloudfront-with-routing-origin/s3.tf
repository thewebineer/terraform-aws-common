
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "${var.site_name}"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.the_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn}"]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["${aws_s3_bucket.the_bucket.arn}"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn}"]
    }
  }
}

resource "aws_s3_bucket" "the_bucket" {
  bucket = "${var.site_name}"
  acl    = "private"

  tags {
    Name = "${var.site_name}"
  }
}

resource "aws_s3_bucket_policy" "the_bucket_policy" {
  bucket = "${aws_s3_bucket.the_bucket.id}"
  policy = "${data.aws_iam_policy_document.s3_policy.json}"
}
