output "site_public_dns" {
  value = "${aws_cloudfront_distribution.s3_distribution.*.domain_name}"
}

output "site_public_dns_geo_restriction" {
  value = "${aws_cloudfront_distribution.s3_distribution_w_geo_restriction_whitelist.*.domain_name}"
}