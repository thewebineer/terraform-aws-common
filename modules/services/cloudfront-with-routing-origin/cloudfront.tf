// todo: compression at the s3 origin or within cloudfront settings
// todo: test cache settings ultimately want to allow everthing to be cached for as long as possible
// minus index.html needs to be set to Cache-Control: public, must-revalidate, proxy-revalidate, max-age=0
// verify this

// todo: need second origin for alb api

resource "aws_cloudfront_distribution" "s3_distribution" {
  count = "${var.use_geo_restriction_whitelist ? 0 : 1}"

  origin {
    domain_name = "${aws_s3_bucket.the_bucket.bucket_regional_domain_name}"
    origin_id   = "${var.site_name}"

    s3_origin_config {
        origin_access_identity = "${aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path}"
    }
  }

  origin {
    domain_name = "${var.origin_domain_name}"
    origin_id   = "${var.origin_name}"
    custom_origin_config {
      http_port = "80"
      https_port = "443",
      origin_protocol_policy = "${var.origin_protocol_policy}"
      origin_ssl_protocols = ["TLSv1","TLSv1.1","TLSv1.2"]
      origin_keepalive_timeout = 5
      origin_read_timeout = 30
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "${var.site_name}"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.site_name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
  }

  # Cache behavior with precedence 0
  ordered_cache_behavior {
    path_pattern     = "/api/*"
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.origin_name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = true
    viewer_protocol_policy = "allow-all"
  }

  price_class = "${var.cloudfront_price_class}"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags {
    Environment = "${var.environment}"
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

resource "aws_cloudfront_distribution" "s3_distribution_w_geo_restriction_whitelist" {
  count = "${var.use_geo_restriction_whitelist ? 1 : 0}"

  origin {
    domain_name = "${aws_s3_bucket.the_bucket.bucket_regional_domain_name}"
    origin_id   = "${var.site_name}"

    s3_origin_config {
        origin_access_identity = "${aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path}"
    }
  }

  origin {
    domain_name = "${var.origin_domain_name}"
    origin_id   = "${var.origin_name}"
    custom_origin_config {
      http_port = "80"
      https_port = "443",
      origin_protocol_policy = "${var.origin_protocol_policy}"
      origin_ssl_protocols = ["TLSv1","TLSv1.1","TLSv1.2"]
      origin_keepalive_timeout = 5
      origin_read_timeout = 30
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "${var.site_name}"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.site_name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
  }

  # Cache behavior with precedence 0
  ordered_cache_behavior {
    path_pattern     = "/api/*"
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.origin_name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = true
    viewer_protocol_policy = "allow-all"
  }

  price_class = "${var.cloudfront_price_class}"

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = "${var.geo_restriction_whitelist}"
    }
  }

  tags {
    Environment = "${var.environment}"
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}