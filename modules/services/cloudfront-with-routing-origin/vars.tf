variable "environment" {
  description = "environment"
}

variable "site_name" {
  description = "site_name"
}

variable "cloudfront_price_class" {
  description = "cloud front price class for distibution caching PriceClass_All , PriceClass_200 (US,CANADA,EUROPE,ASIA,AFRICA), PriceClass_100 (US,CANADA,EUROPE)"
  default = "PriceClass_100"
}

variable "geo_restriction_whitelist" {
  default = ["US", "CA", "GB", "DE"]
  type = "list"
}

variable "use_geo_restriction_whitelist" {
  default = false
}

variable "origin_name" {
  
}

variable "origin_domain_name" {
  
}

variable "origin_protocol_policy" {
  description = "The origin protocol policy to apply to your origin. One of http-only, https-only, or match-viewer"
  default = "http-only"
}
