variable "aws_region" {
  description = "The AWS region to use"
}

variable "ami_id" {
  description = "The ami used for the compute"
}


variable "environment" {
  description = "environment"
}

variable "vpc_cidr" {
  description = "vpc cidr block"
}

variable "public_subnets_cidr" {
    description = "public subnets"
    type = "list"
}

variable "private_subnets_cidr" {
    description = "private subnets"
    type = "list"
}

variable "ecs_key_pair_name" {
  description = "key pair for ssh into machines"
}

variable "max_instance_size" {
    description = "max number of machines to run in cluster"
}

variable "min_instance_size" {
    description = "min number of machines to run in cluster"
}

variable "desired_capacity" {
    description = "The number of Amazon EC2 instances that should be running in the group"
}

variable "ecs_cluster_name" {
  description = "Cluster Name"
}

