output "ecs_cluster_id" {
  value = "${aws_ecs_cluster.ecs_cluster.id}"
}

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "public_sg_id" {
  value = "${aws_security_group.public_sg.id}"
}

output "public_subnet_ids" {
  value = "${aws_subnet.public_subnet.*.id}"
}
