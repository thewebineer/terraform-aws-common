resource "aws_waf_ipset" "ipset" {
  name = "${var.environment}-${var.name}-ipset"

  ip_set_descriptors {
    type  = "IPV4"
    value = "${var.cidr_to_allow}"
  }
}

resource "aws_waf_rule" "wafrule" {
  depends_on  = ["aws_waf_ipset.ipset"]
  name        = "${var.environment}-${var.name}-rule"
  metric_name = "${var.environment}-${var.name}-rule-metric"

  predicates {
    data_id = "${aws_waf_ipset.ipset.id}"
    negated = false
    type    = "IPMatch"
  }
}

resource "aws_waf_web_acl" "waf_acl" {
  depends_on  = ["aws_waf_ipset.ipset", "aws_waf_rule.wafrule"]
  name        = "${var.environment}-${var.name}-ACL"
  metric_name = "${var.environment}-${var.name}-ACL-metric"

  default_action {
    type = "BLOCK"
  }

  rules {
    action {
      type = "ALLOW"
    }

    priority = 1
    rule_id  = "${aws_waf_rule.wafrule.id}"
    type     = "REGULAR"
  }
}