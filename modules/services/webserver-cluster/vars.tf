
variable "server_port" {
  description = "The port of the server that will be sued for HTTP requests"
  default = 8080
}

variable "cluster_name" {
  description = "The name to use for all the cluster resources"
}

variable "db_remote_state_bucket" {
  description = "The name of the S3 bucket for the database's remote state"
}

variable "db_remote_state_key" {
  description = "The path for the database's remote state in S3"
}

variable "enable_autoscaling" {
  description = "If set to true, enable auto scaling"
}

variable "ami" {
  description = "The AMI to run in the cluster"
  default = "ami-40d28157"
}

variable "server_text" {
  description = "The text the web server should return"
  default = "Hello, World"
}

variable "instance_type" {
  description = "The instance type"
  default = "t2.micro"
}

variable "min_size" {
  description = "The min number of servers"
  default = 2
}

variable "max_size" {
  description = "The min number of servers"
  default = 10
}

variable "aws_region" {
  description = "The AWS region to use"
}

