
module "ecs" {
  source = "../../../modules/services/ecs"
  
  aws_region = "${var.aws_region}"
  vpc_id = "${data.terraform_remote_state.ecs_network.vpc_id}"
  public_sg_id = "${data.terraform_remote_state.ecs_network.public_sg_id}"
  public_subnet_ids = "${data.terraform_remote_state.ecs_network.public_subnet_ids}"
  ecs_cluster_id = "${data.terraform_remote_state.ecs_network.ecs_cluster_id}"
  task_name = "${var.task_name}"
  container_port = "${var.container_port}"
  ecr_repository_url = "${data.terraform_remote_state.ecr.repository_url}:${var.ecr_repository_tag}"
  environment = "${var.environment}"
}

provider "aws" {
    region = "${var.aws_region}"
}

data "terraform_remote_state" "ecr" {
  backend = "s3"
  config {
    bucket = "${var.ecr_remote_state_bucket}"
    key    = "${var.ecr_remote_state_key}"
    region = "${var.aws_region}"
  }
}

data "terraform_remote_state" "ecs_network" {
  backend = "s3"
  config {
    bucket = "${var.ecs_network_remote_state_bucket}"
    key    = "${var.ecs_network_remote_state_key}"
    region = "${var.aws_region}"
  }
}