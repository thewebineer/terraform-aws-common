variable "environment" {
  description = "environment"
  default = "ci"
}

variable "site_name" {
  description = "site_name"
  default = "sitemanager"
}

variable "aws_region" {
  description = "The AWS region to use"
  default = "us-east-1"
}