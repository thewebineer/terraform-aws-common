module "cloudfront" {
  source = "../../../modules/services/cloudfront"
  
  site_name = "${var.environment}.${var.site_name}.com"
  environment = "${var.environment}"
}

provider "aws" {
    region = "${var.aws_region}"
}