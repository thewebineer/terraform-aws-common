
module "ecs" {
  source = "../../../modules/services/ecs-cluster"
  
  ami_id = "${data.aws_ami.ecs.id}"
  ecs_cluster_name = "${var.ecs_cluster_name}"
  desired_capacity = "${var.desired_capacity}"
  min_instance_size = "${var.min_instance_size}"
  max_instance_size = "${var.max_instance_size}"
  ecs_key_pair_name = "${var.ecs_key_pair_name}"
  private_subnets_cidr = "${var.private_subnets_cidr}"
  public_subnets_cidr = "${var.public_subnets_cidr}"
  vpc_cidr = "${var.vpc_cidr}"
  environment = "${var.environment}"
  aws_region = "${var.aws_region}"
}

provider "aws" {
    region = "${var.aws_region}"
}

data "aws_ami" "ecs" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name = "architecture"
    values = ["x86_64"]
  }

  filter {
    name = "image-type"
    values = ["machine"]
  }

  filter {
    name = "name"
    values = ["*amazon-ecs-optimized"]
  }
  
}
