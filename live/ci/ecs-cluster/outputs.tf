output "ecs_cluster_id" {
  value = "${module.ecs.ecs_cluster_id}"
}

output "vpc_id" {
  value = "${module.ecs.vpc_id}"
}

output "public_sg_id" {
  value = "${module.ecs.public_sg_id}"
}

output "public_subnet_ids" {
  value = "${module.ecs.public_subnet_ids}"
}
