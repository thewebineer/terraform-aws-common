module "webserver_cluster" {
  source = "../../../modules/services/webserver-cluster"
  
  ami = "${data.aws_ami.ubuntu.id}" # ami-40d28157
  server_text = "foo bar"
  cluster_name = "webservers-stage"
  db_remote_state_bucket = "thewebineer-terraform-state"
  db_remote_state_key = "stage/data-stores/mysql/terraform.tfstate"
  enable_autoscaling = "false"
  aws_region = "${var.aws_region}"
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["099720109477"] # Canonical

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name = "architecture"
    values = ["x86_64"]
  }

  filter {
    name = "image-type"
    values = ["machine"]
  }

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }
  
}


provider "aws" {
    region = "${var.aws_region}"
}

resource "aws_security_group_rule" "allow_testing_inbound" {
  type = "ingress"
  security_group_id = "${module.webserver_cluster.elb_security_group_id}"

  from_port = 12345
  to_port = 12345
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}
