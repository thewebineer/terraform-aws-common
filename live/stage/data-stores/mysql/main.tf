terraform {
  backend "s3" {
      bucket = "thewebineer-terraform-state"
      key = "stage/data-stores/mysql/terraform.tfstate"
      region = "us-east-1"
      encrypt = true
  }
}

provider "aws" {
    region = "us-east-1"
}

data "terraform_remote_state" "network" {
  backend = "s3"
  config {
    bucket = "thewebineer-terraform-state"
    key    = "stage/data-stores/mysql/terraform.tfstate"
    region = "us-east-1"
  }
}

resource "aws_db_instance" "example" {
    engine = "mysql"
    allocated_storage = 10
    instance_class = "db.t2.micro"
    name = "example_database"
    username = "admin"
    password = "${var.db_password}"
}
