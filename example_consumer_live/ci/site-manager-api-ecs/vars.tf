variable "aws_region" {
  description = "The AWS region to use"
  default = "us-east-1"
}

variable "ecr_repository_tag" {
  description = "tag for docker image"
  default = "latest"
}


variable "environment" {
  description = "environment"
  default = "ci"
}

variable "container_port" {
  description = "container_port"
  default = 5000
}

variable "task_name" {
  description = "task name"
  default = "site-manager-api"
}

variable "ecr_remote_state_bucket" {
  description = "the remote state storage bucket for the ecr repository"
  default = "thewebineer-terraform-state"
}

variable "ecr_remote_state_key" {
  description = "the remote state key for the ecr repository"
  default = "global/ecr-site-manager-api/terraform.tfstate"
}

variable "ecs_network_remote_state_bucket" {
  description = "the remote state storage bucket for the ecr repository"
  default = "thewebineer-terraform-state"
}

variable "ecs_network_remote_state_key" {
  description = "the remote state key for the ecr repository"
  default = "ci/ecs-cluster/terraform.tfstate"
}