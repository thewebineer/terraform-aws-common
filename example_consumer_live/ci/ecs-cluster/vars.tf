variable "aws_region" {
  description = "The AWS region to use"
  default = "us-east-1"
}

variable "environment" {
  description = "environment"
  default = "ci"
}

variable "vpc_cidr" {
  description = "vpc cidr block"
  default = "10.10.0.0/16"
}

variable "public_subnets_cidr" {
    description = "private subnets"
    default = [
        "10.10.10.0/24",
        "10.10.20.0/24"
    ]
}

variable "private_subnets_cidr" {
    description = "private subnets"
    default = [
        "10.10.30.0/24",
        "10.10.40.0/24"
    ]
}

variable "ecs_key_pair_name" {
  description = "key pair for ssh into machines"
  default = "ec2keypair"
}

variable "max_instance_size" {
    description = "max number of machines to run in cluster"
    default = 10
}

variable "min_instance_size" {
    description = "min number of machines to run in cluster"
    default = 2
}

variable "desired_capacity" {
    description = "The number of Amazon EC2 instances that should be running in the group"
    default = 2
}

variable "ecs_cluster_name" {
  description = "Cluster Name"
  default = "TheWebineerCluster"
}

