
module "ecr" {
  source = "../../modules/services/ecr"
  
  repository_name = "${var.repository_name}"
  aws_region = "${var.aws_region}"
}

provider "aws" {
    region = "${var.aws_region}"
}
