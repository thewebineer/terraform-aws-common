terraform {
  backend "s3" {
      bucket = "thewebineer-terraform-state"
      key = "global/ecr-site-manager-api/terraform.tfstate"
      region = "us-east-1"
      dynamodb_table = "thewebineer-terraform-book-lock"
      encrypt = true
  }
}
