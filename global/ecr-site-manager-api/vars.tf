
variable "aws_region" {
  description = "The AWS region to use"
  default = "us-east-1"
}

variable "repository_name" {
  description = "ecr repository name"
  default = "site-manager-api"
}
