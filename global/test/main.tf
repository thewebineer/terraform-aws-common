provider "aws" {
    region = "us-east-1"
}

module "s3" {
  source = "../../modules/services/s3"
}

resource "aws_s3_bucket" "terraform_state" {
    bucket = "test2"
}